import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        StringBuffer newName = new StringBuffer(name);

        for (int i = 0; i < name.length(); i++) {
            if (Character.isLowerCase(name.charAt(i))) {
                newName.setCharAt(i, Character.toUpperCase(name.charAt(i)));
            } else if (Character.isUpperCase(name.charAt(i))) {
                newName.setCharAt(i, Character.toLowerCase(name.charAt(i)));
            }
        }
        System.out.println("Cevrilmis soz " + newName);
    }
}
