public class Main {
    public static void main(String[] args) {
        String t = ("             1,e 6,e 0,e 9,e           ");
        t = t.replaceAll("[\\s,e]+", "");
        String[] numbers = t.split("");
        System.out.println("Numbers that we got");
        for (String number : numbers) {
            int num = Integer.parseInt(number);
            System.out.println(num * num);
        }
    }
}
