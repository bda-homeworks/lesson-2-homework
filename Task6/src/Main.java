
public class Main {
    public static void main(String[] args) {
        System.out.printf("Hello and welcome! Men bu gun min ve max qiymetlerini gosterem primitive typelerin");
        short a = 10;
        byte b = 20;
        float c = 20.20f;
        long d = 3_000_000_00;
        System.out.println("Shortun maximal qiymeti" + Short.MAX_VALUE);
        System.out.println("Shortun minimal qiymeti" + Short.MIN_VALUE);
        System.out.println("Byte maximal qiymeti" + Byte.MAX_VALUE);
        System.out.println("Byte minimal qiymeti" + Byte.MIN_VALUE);
        System.out.println("Float maximal qiymeti" + Float.MAX_VALUE);
        System.out.println("Float minimal qiymeti" + Float.MIN_VALUE);
        System.out.println("Longun maximal qiymeti" + Long.MAX_VALUE);
        System.out.println("Longun minimal qiymeti" + Long.MIN_VALUE);
    }
}